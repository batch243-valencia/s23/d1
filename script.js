console.log("Hello");

let cellphone = {
    name: "Nokia 3310",
    manufactureDate: 1999
}

console.log(cellphone);

// syntax
    // function objectName(valueA, valueB){
        // this.keyA = valueA,
        // this.keyB= valueB}
    
// this is an contructor function
// this keywords allows us to assign new object properties by associating them with values received from a constructor function's parameter's
    function Laptop(name,manufactureDate){
        this.laptopName=name;
        this.laptopManufactureDate=manufactureDate;
        this.LaptopRam;
    }

// Instatiation
// the "new" operator create an instance of an object
// Objects and instances are ofter interchange because object literals(let object = {}) and instances (let objectName = new functionName(arguments))are distinct/unique object

let laptop = new Laptop('Lenovo', 2008, "4gb");
console.log("Result from creating objects using object Constructor");
console.log(laptop);
let myLaptop = new Laptop('MacBook Air', 2020, "8gb");
console.log("Result from creating objects using object Constructor");
console.log(myLaptop);

let oldLaptop = Laptop("Portal r2e CCMC",1980,"500mb");

console.log("Result from creating objects w/o the new keyword;")
console.log(oldLaptop);

/*Mini-Activity*/

function Menu(desc,price){
    this.description=desc;
    this.price=price;
}


let Menu1 = new Menu ("adobo", 150);
console.log(Menu1);//Menu {description: 'adobo', price: 150}

let Menu2 = new Menu("sinigang", 140);
console.log(Menu2);//Menu {description: 'sinigang', price: 140}

// creating empty objects
let computer = {};
let myComputer = new Object();
console.log(computer);//{}
console.log(myComputer);//{}

let array = [laptop,myLaptop];
console.log(array);// [Laptop, Laptop]
console.log(array[0]);//{laptopName: 'Lenovo', laptopManufactureDate: 2008}
// 
console.log(array[0]['laptopName']);//lenovo
// dot notation
console.log(array[0].laptopName);//lenovo


// [Section]Initializing/adding/deleting/reassining Object Properties.
let car = {};
// Initializing/ adding object properties using dot notation
car.name = "Honda Civic";
console.log(car);
// Initializing/adding object using bracket[] notaion
car['manufactureDate'] = 2019;

console.log(car);
// deleting object properties using [bracket] notation
    // delete car["name"];
// deleting object properties using .dot notation
    delete car.manufactureDate;

console.log(car);

// reassign object properties
        // reassign object .dot notation
            car.name= "Dodge Charger R/T";
            console.log(car);
            car["name"] = "Jeepney";
            console.log(car);
// [Section] object methods

let person = {
    name:'john',
    talk:function(){
        console.log("Hello my name is " + this.name);
    }
}

console.log(person);
person.talk();

person.walk=function(){
    console.log(this.name+" walked 25 steps forward.");
}
person.talk();


let friends = {
    firstName: 'Joe',
    lastName: 'Smith',
    address: {
        city:'Austin',
        country:'Texas'
    },
    emails:['joe@gmail.com','joesmith@gemail.xyz'],
    phoneNumber:[[09123456789],[04395432154]],
    introduce:function(){
        console.log("Hello my name is "+ this.firstName+" "+this.lastName+ ". I live in "+this.address.city+" "+this.address.country+ ". My email are "+ this.emails[0]+ " and "+this.emails[1]+". My Numbers are "+ this.phoneNumber[0]+" "+this.phoneNumber[1]);
    }
}
friends.introduce();

function Pokemon(name,level){
    // Properties Pokemon
    this.pokemonName = name;
    this.pokemonLevel= level;
    this.pokemonHealth= 2*level;
    this.pokemonAttack=level;
    // methods
    this.tackle=function(targetPokemon){
        console.log(this.pokemonName+"Used Tackle on "+targetPokemon.pokemonName);
        console.log(targetPokemon.pokemonName+"is reduced by "+this.pokemonAttack);
    }
}
let pikachu = new Pokemon("Pickachu", 12);
let gyarados = new Pokemon("gyarados", 11);
pikachu.tackle(gyarados);
